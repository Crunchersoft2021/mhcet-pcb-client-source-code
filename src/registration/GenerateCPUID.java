
package registration;

import java.security.NoSuchAlgorithmException;
import javax.swing.JOptionPane;

public class GenerateCPUID {
    String CPUID;
    String motherboard;
    public String generateCPUID()
    {
        motherboard=MiscUtils.getMotherboardSN();
        try {
            CPUID=new HashCodeGenerator().calculateSecurityHash(motherboard,new ChangeAlgorithm().changeAlgorithm());
        } catch (NoSuchAlgorithmException ex) {
            JOptionPane.showMessageDialog(null,"Unable To Retrieve CPU ID");
        }
        return CPUID;
    }
}