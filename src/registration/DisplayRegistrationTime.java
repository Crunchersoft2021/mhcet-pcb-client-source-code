/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package registration;

import server.DBConnection1;
import server.Server;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 007
 */
public class DisplayRegistrationTime {
//    ResultSet rs;
//    Statement st;
    Timestamp t1=null;
    int rollNo;
    public DisplayRegistrationTime(int roll)
    {
        this.rollNo=roll;
    }
    public long displayRegistration()
    {
        long registrationTime=0;
        long i=(long) 31536000000.0;
        
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        Timestamp t1=null;
        try {
            Statement st=conn.createStatement();
            rs=st.executeQuery("select * from Register");
            while(rs.next())
                    {
                        registrationTime=rs.getLong(4);
                    }
            System.out.print(registrationTime);
        } catch (SQLException ex) {
            Logger.getLogger(CreateTableRegistration.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CreateTableRegistration.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return registrationTime;
    }
    public static void main(String s[])
    {
        //new DisplayRegistrationTime().displayRegistration();
    }
}