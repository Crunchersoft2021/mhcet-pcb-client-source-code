package NEET.practice;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import NEET.question.QuestionBean;
import server.DBConnection1;
import server.Server;

public class DBConnection {
    
//    private static String driver = "org.apache.derby.jdbc.EmbeddedDriver"; 
//    private static String protocol = "jdbc:derby:CETNeetNew;create=true;dataEncryption=true;bootPassword=*007_Worlds+Best+Company@Wagholi_007*";
//    Connection con1,con2,conn;
//    Statement s,st1,st2;
    String hintbtn;
//    ResultSet rs,rs1,rs2;
    
    public DBConnection()
    {
        
    }    
    
    public static void delTest(int testId) {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();
            String sql="delete from Saved_Test_Info where id=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, testId);
            int i=ps.executeUpdate();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public String getChapName(int id){
        String name=null;
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement(); 
            String query = "Select Chapter_Name from Chapter_Info where Chapter_Id = "+id;
            rs=s.executeQuery(query);
            if(rs.next())
            {
                name = rs.getString(1);
            }
            return name;
        } catch (Exception ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return name;
    }
    
    public void addResult(PracticeResultBean practiceResultBean,int rollNo)
    {
        if(practiceResultBean!=null)
        {            
            Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
            ResultSet rs;
            try
            {  
                Statement s=conn.createStatement();
                String sql="insert into Practice_Result_Info values(?,?,?,?)";
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setInt(1,practiceResultBean.getPracticeId());
                ps.setInt(2, rollNo);
                ps.setInt(3,practiceResultBean.getTotalQuestions());
                ps.setInt(4,practiceResultBean.getCorrectQuestions());
                
                int r = ps.executeUpdate();
                if(r>0)
                {
                    //System.out.println("Practice Result Added");
                }
                else
                {
                    //System.out.println("Problem in Result Adding");
                }
            } 
            catch (Exception ex) 
            {
                //JOptionPane.showMessageDialog(null, ex.getMessage());
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }
    }
    
    public int getPerwrongQuestionMarks(int subjectId)
    { 
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();  
            rs = s.executeQuery("Select Marks_Per_Wrong_Question from Subject_Info where Subject_Id="+subjectId);           
            if(rs.next())
            {                
                return rs.getInt(1);
            }            
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }
    
    public int getPerQuestionMarks(int subjectId)
    { 
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();  
            rs = s.executeQuery("Select * from Subject_Info where Subject_Id="+subjectId);           
            if(rs.next())
            {                
                return rs.getInt(3);
            }            
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }
    
    public ArrayList<QuestionBean> selectQuestionsForPractice(int subjectId,int chapterId,int questionCount)
    {        
        //resetQuestionAttempt(subjectId, questionCount);
        int cnt=10;
        ArrayList<QuestionBean> al = getQuestionsForPractice(subjectId,chapterId);        
        if(al!=null)
        {   
            Collections.shuffle(al);
            ArrayList<QuestionBean> alFinal=new ArrayList<QuestionBean>();
            for (int idx = 0; idx < cnt; ++idx)
            {                
                alFinal.add(al.get(idx));
            }        
            return alFinal;
        }
        return null;
    }
    
    public PracticeBean getPracticeBean(int subjectId,int chapterId)
    {
        int questionCount=0,totalTime=0;
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();
            String query = "Select * from Group_info where group_Id = "+subjectId;
            rs=s.executeQuery(query);
            if(rs.next())
            {
                switch(subjectId)
                {
                    case 1:questionCount = rs.getInt(3);break;
                    case 2:questionCount = rs.getInt(4);break;
                    case 3:questionCount = rs.getInt(5);break;
                }                
                totalTime = rs.getInt(6);
            }
            ArrayList<QuestionBean> alQuestions=selectQuestionsForPractice(subjectId,chapterId,questionCount);
            PracticeBean practiceBean=new PracticeBean();
            practiceBean.setSubjectId(subjectId);
            practiceBean.setChapterId(chapterId);
            practiceBean.setTotalTime(totalTime/2);
            practiceBean.setRemainingTime(totalTime);
            practiceBean.setQuestions(alQuestions);
            return practiceBean;
        } catch (Exception ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }        
   
    private ArrayList<QuestionBean> getQuestionsForPractice(int subjectId,int chapterId)
    {                    
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs1;
        try
        {  
            Statement s=conn.createStatement();
            ArrayList<QuestionBean> al=new ArrayList<QuestionBean>();
            rs1 = s.executeQuery("Select distinct * from Question_Info where Subject_Id ="+subjectId+" and Chapter_Id ="+chapterId+" order by Question_Id");          
            while(rs1.next())
            {
                String question,A,B,C,D,Answer,Hint,optionImagePath,QuestionImagePath,hintImagePath;
                int sub_Id,Chap_Id,Top_Id,question_Id,attempt;
                question_Id=rs1.getInt(1);
                question=rs1.getString(2);
                A=rs1.getString(3);
                B=rs1.getString(4);
                C=rs1.getString(5);
                D=rs1.getString(6);
                Answer=rs1.getString(7);
                Hint=rs1.getString(8);                
                sub_Id=rs1.getInt(9);
                Chap_Id=rs1.getInt(10);
                Top_Id=rs1.getInt(11);
                QuestionImagePath=rs1.getString(13);
                optionImagePath=rs1.getString(15);
                hintImagePath=rs1.getString(17);
                boolean result = (rs1.getInt(12)==0)?false:true;
                boolean result1 = (rs1.getInt(14)==0)?false:true;               
                boolean result2 = (rs1.getInt(16)==0)?false:true;               
                attempt=rs1.getInt(18);
                QuestionBean q=new QuestionBean(question_Id, question, A, B, C, D, Answer, Hint, sub_Id, Chap_Id, Top_Id, result, QuestionImagePath, result1, optionImagePath,result2,hintImagePath,attempt);
                al.add(q);    
            }    
            return al;
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    public String hintreturn()
    {
        return hintbtn;
    }
     
    public void finishPractice(int practiceId)
    {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();
            int r = s.executeUpdate("Update Practice_Info set status='Finished' where Practice_Id="+practiceId);
            if(r>0)
            {
                //System.out.println("Practice finished");
            }
        }
        catch(Exception e)
        {
            
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
        
    public void saveStateOfNewPractice(PracticeBean practiceBean,int rollNo)
    {
        int practiceId=0;
        if(practiceBean!=null)
        {
            Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();
                rs = s.executeQuery("Select max(Practice_Id) from Practice_Info");
                if(rs.next())
                {
                        practiceId=rs.getInt(1);
                }
                practiceId++;
                java.util.Date date = new java.util.Date();
                SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String strDate=formatter.format(date);
                PreparedStatement ps=conn.prepareStatement("insert into Practice_Info values(?,?,?,?,?,?)");
                ps.setInt(1, practiceId);
                ps.setInt(2, rollNo);
                ps.setInt(3, practiceBean.getSubjectId());
                ps.setInt(4, practiceBean.getChapterId());
                ps.setString(5,"Started");                
                ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
                int executeUpdate = ps.executeUpdate();
                practiceBean.setPracticeId(practiceId);
                ArrayList<QuestionBean> alQuestions=practiceBean.getQuestions();
                Iterator<QuestionBean> it = alQuestions.iterator();
                while(it.hasNext())
                {
                    QuestionBean question = it.next();                    
                    ps=conn.prepareStatement("insert into Practice_Question_Status_Info values(?,?,?)");
                    ps.setInt(1, practiceId);
                    ps.setInt(2, question.getQuestion_Id());
                    ps.setString(3,"unAttempted");
                    ps.executeUpdate();                    
                }
                practiceBean.setStatus("Started");     
                practiceBean.setStartDateTime(new java.util.Date());
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }
    }
    
    public void setQuestionStatus(QuestionBean question,int practiceId, String userAnswer)
    {
        if(question!=null)
        {
            Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();
                s.executeUpdate("update Practice_Question_Status_Info set user_Answer='"+userAnswer+"' where Question_Id="+question.getQuestion_Id()+" and practice_Id="+practiceId);
                
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }
    }
    
    public String getResult(int practiceId,int questionId)
    {        
            Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();
                rs = s.executeQuery("Select user_Answer from  Practice_Question_Status_Info where practice_Id="+practiceId+" and Question_Id="+questionId);   
                if(rs.next())
                {
                    return rs.getString(1);
                }                
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
            return null;
    }
   
    public static void main(String s1[])
    {
        DBConnection s=new DBConnection();        
    }

    public ArrayList<String> getClassTestInfo(int index) {
        ArrayList<String> testInfo=new ArrayList<String>();
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
            ResultSet rs;
            try
            {  
                Statement s=conn.createStatement();
                rs = s.executeQuery("Select * from  Class_Test_Info where group_Id="+index);   
                while(rs.next())
                {
                    String temp=rs.getInt(1)+"\t"+rs.getBoolean(3)+"\t"+rs.getTimestamp(4);
                    testInfo.add(temp);
                }                
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return testInfo;
    }

    public int getQuestionCount(int testId) {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
            ResultSet rs;
            try
            {  
                Statement s=conn.createStatement();
                rs = s.executeQuery("Select count(*) from  Class_Test_Question_Info where testId="+testId);   
                while(rs.next())
                {
                    return rs.getInt(1);
                }                
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return -1;
    }

    public int getMarks(int index) {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
            ResultSet rs;
            try
            {  
                Statement s=conn.createStatement();
                rs = s.executeQuery("Select Marks_Per_Question from  Subject_Info where Subject_Id="+index);   
                while(rs.next())
                {
                    return rs.getInt(1);
                }                
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
            return -1;
    }
}