/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NEET.practice;

/**
 *
 * @author Avadhut
 */
public class PracticeResultBean {
    
    private int practiceId;
    private int totalQuestions;
    private int correctQuestions;

    public PracticeResultBean() {
    }

    public PracticeResultBean(int practiceId, int totalQuestions, int correctQuestions) {
        this.practiceId = practiceId;
        this.totalQuestions = totalQuestions;
        this.correctQuestions = correctQuestions;
    }
    
    public int getCorrectQuestions() {
        return correctQuestions;
    }

    public void setCorrectQuestions(int correctQuestions) {
        this.correctQuestions = correctQuestions;
    }

    public int getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(int practiceId) {
        this.practiceId = practiceId;
    }

    public int getTotalQuestions() {
        return totalQuestions;
    }

    public void setTotalQuestions(int totalQuestions) {
        this.totalQuestions = totalQuestions;
    } 
}