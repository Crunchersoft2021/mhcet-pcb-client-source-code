/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NEET.unitTest;

/**
 *
 * @author 007
 */
public class UnitTestResultBean {
    
    private int unitTestId;
    private int totalQuestions;
    private int correctQuestions;

    public UnitTestResultBean() {
    }

    public UnitTestResultBean(int unitTestId, int totalQuestions, int correctQuestions) {
        this.unitTestId = unitTestId;
        this.totalQuestions = totalQuestions;
        this.correctQuestions = correctQuestions;
    }
    
    public int getCorrectQuestions() {
        return correctQuestions;
    }

    public void setCorrectQuestions(int correctQuestions) {
        this.correctQuestions = correctQuestions;
    }

    public int getUnitTsetId() {
        return unitTestId;
    }

    public void setUnitTestId(int practiceId) {
        this.unitTestId = unitTestId;
    }

    public int getTotalQuestions() {
        return totalQuestions;
    }

    public void setTotalQuestions(int totalQuestions) {
        this.totalQuestions = totalQuestions;
    }
}